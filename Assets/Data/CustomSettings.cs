﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CustomSettings
{
    private static bool isEnglish;

    private static int currentHotSpot = 1;

    public static bool IsEnglish
    {
        get
        {
            return isEnglish;
        }
        set
        {
            isEnglish = value;
        }
    }

    public static int CurrentHotSpot 
    {
        get{
            return currentHotSpot;
        }
        set
        {
            currentHotSpot = value;
        }
    }
}